#!/usr/bin/env python

import re, sys, os, math, pprint, operator

player_regex = re.compile(r"((\w+ \w+) batted (?P<bat>\d+?) times with (?P<hit>\d+?) hits and (?P<run>\d+?) runs)")

def get_data(text):
    return player_regex.findall(text)
    
def print_data(matches):
    players = {}
    for k in matches:
        if k[1] in players.keys():
            bat, hit, run = players[k[1]]
            players[k[1]] = (int(bat)+ int(k[2]), int(hit)+int(k[3]), int(run)+int(k[4]))
        else:
            players[k[1]] =(k[2], k[3], k[4])
    return players
    
def batting_average(player_stats):
    BA = {}
    for k, v in player_stats.items():
        BA[k] = round(float(v[1]/float(v[0])), 3)
    sortBA = sorted(BA.iteritems(), key=operator.itemgetter(1), reverse=True)
    with open("output.txt", "w") as f:
        f.write('\n'.join('%s: %s' % x for x in sortBA))   
    pprint.pprint(sortBA) 


if len(sys.argv) < 2:
    sys.exit("Usage: %s filename" % sys.argv[0])
 
filename = sys.argv[1]
 
if not os.path.exists(filename):
    sys.exit("Error: File '%s' not found" % sys.argv[1])

f = open(filename)
file_contents = f.read()

matches = get_data(file_contents)

player_stats = print_data(matches)

batting_average(player_stats)

f.close()